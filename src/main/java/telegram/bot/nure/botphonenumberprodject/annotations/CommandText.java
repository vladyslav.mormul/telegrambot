package telegram.bot.nure.botphonenumberprodject.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
public @interface CommandText {
    boolean required() default false;

    String message() default "Invalid text";

    String defaultText() default "Default text";
}
