package telegram.bot.nure.botphonenumberprodject.filter;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserUniqueFilter {
    private Long chatId;
    private Long userId;
    private String phoneNumber;

    public boolean areAllFieldsNull() {
        return this.chatId == null && this.phoneNumber == null && this.userId == null;
    }
}
