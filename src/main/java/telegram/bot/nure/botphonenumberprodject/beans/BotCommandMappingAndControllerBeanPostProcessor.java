package telegram.bot.nure.botphonenumberprodject.beans;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.context.ApplicationContext;
import org.springframework.core.env.Environment;
import telegram.bot.nure.botphonenumberprodject.annotations.BotCommandController;
import telegram.bot.nure.botphonenumberprodject.annotations.BotCommandMapping;
import telegram.bot.nure.botphonenumberprodject.converters.ParameterConverter;
import telegram.bot.nure.botphonenumberprodject.converters.TypeStringConverter;
import telegram.bot.nure.botphonenumberprodject.finder.*;
import telegram.bot.nure.botphonenumberprodject.handle.HandleRequest;
import telegram.bot.nure.botphonenumberprodject.handle.InvokeCommandMethod;
import telegram.bot.nure.botphonenumberprodject.handle.SetUpdateHandleRequest;

import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class BotCommandMappingAndControllerBeanPostProcessor implements BeanPostProcessor {
    private final List<ParameterFinder> parameterFinders;
    private final InvokeCommandMethod commandAndMethod;
    private final Environment env;

    public BotCommandMappingAndControllerBeanPostProcessor(ApplicationContext context, InvokeCommandMethod commandAndMethod, Environment env) {
        this.commandAndMethod = commandAndMethod;
        this.env = env;
        this.parameterFinders = List.of(
                new UpdateFinder(),
                new ChatIdFinder(),
                new CommandTextFinder(),
                new ValueAnnotationFinder(new TypeStringConverter(), this::getValue),
                new QualifierBeanFinder(context),
                new BeanFinder(context)
        );
    }

    @Override
    public Object postProcessBeforeInitialization(Object bean, @NotNull String beanName) throws BeansException {
        if (bean.getClass().getAnnotation(BotCommandController.class) == null) {
            return bean;
        }

        BotCommandMapping mapping;
        for (final Method method : bean.getClass().getMethods()) {
            mapping = method.getAnnotation(BotCommandMapping.class);

            if (mapping == null) {
                continue;
            }

            for (String command : mapping.commands()) {
                commandAndMethod.put(checkCommand(command), generateHandle(method, bean, method.getParameters()));
            }
        }

        return bean;
    }

    public String checkCommand(String command) {
        String propertyCommand = getValue(command);
        if (propertyCommand == null) {
            throw new RuntimeException("Property " + command + " was not found");
        }
        if (commandAndMethod.containsCommand(propertyCommand)) {
            throw new RuntimeException("Duplication command = " + propertyCommand);
        }
        return propertyCommand;
    }

    public String getValue(String str) {
        if (!(str.startsWith("${") && str.endsWith("}"))) {
            return str;
        }
        str = env.getProperty(str.substring(2, str.length() - 1));
        return str;
    }

    public HandleRequest generateHandle(Method method, Object controller, Parameter[] methodParameters) {
        final Object[] parameters = new Object[method.getParameters().length];
        final List<ParameterConverter> parameterConverters = Collections.synchronizedList(new ArrayList<>());
        ParameterFinder.Result result;
        for (int i = 0; i < parameters.length; i++) {
            for (var finder : parameterFinders) {
                result = finder.find(methodParameters[i], i);
                if (!result.isSuccess()) {
                    continue;
                }
                if (result.hasConverter()) {
                    parameterConverters.add(result.converter());
                }
                parameters[i] = result.value();
                break;
            }
        }
        return new SetUpdateHandleRequest(
                method, controller, parameters,
                parameterConverters
        );
    }

}



