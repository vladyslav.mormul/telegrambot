package telegram.bot.nure.botphonenumberprodject.service;

import lombok.RequiredArgsConstructor;
import org.telegram.telegrambots.extensions.bots.commandbot.TelegramLongPollingCommandBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.MessageEntity;
import org.telegram.telegrambots.meta.api.objects.Update;
import telegram.bot.nure.botphonenumberprodject.dataclass.User;
import telegram.bot.nure.botphonenumberprodject.events.impls.EditUserEvent;
import telegram.bot.nure.botphonenumberprodject.filter.UserUniqueFilter;
import telegram.bot.nure.botphonenumberprodject.tools.BotUtils;
import telegram.bot.nure.botphonenumberprodject.tools.ContactReplyKeyboardBuilder;
import telegram.bot.nure.botphonenumberprodject.tools.UpdateUtils;

import java.util.List;

@RequiredArgsConstructor
public class TelegramUserServiceImpl implements TelegramUserService {
    private final static String PHONE_NUMBER_TEXT = "Phone number -> ";

    private final TelegramLongPollingCommandBot bot;
    private final UserService userService;
    private final EventManager events;
    private final ContactReplyKeyboardBuilder contactKeyboardBuilder;

    @Override
    public void editUser(Long chatId) {
        BotUtils.sendMessage(
                bot,
                SendMessage.builder()
                        .chatId(chatId)
                        .text("Please share contact")
                        .replyMarkup(contactKeyboardBuilder.build()).build()
        );

        events.put(chatId, new EditUserEvent(userService));
    }

    @Override
    public void showPhoneNumber(Update update) {
        var chatId = UpdateUtils.getChatId(update);

        var phoneNumber = userService
                .findOne(
                        UserUniqueFilter
                                .builder()
                                .chatId(chatId)
                                .build()
                )
                .map(User::getPhoneNumber)
                .orElse("not found");

        BotUtils.sendMessage(bot, phoneNumberMessage(phoneNumber, chatId));
    }

    private SendMessage phoneNumberMessage(String phoneNumber, Long chatId) {
        return SendMessage
                .builder()
                .text(PHONE_NUMBER_TEXT + phoneNumber)
                .entities(boldStyle(phoneNumber.length()))
                .chatId(chatId)
                .build();
    }

    private List<MessageEntity> boldStyle(int textLength) {
        return List.of(
                MessageEntity
                        .builder()
                        .type("bold")
                        .offset(PHONE_NUMBER_TEXT.length())
                        .length(textLength)
                        .build());
    }
}
