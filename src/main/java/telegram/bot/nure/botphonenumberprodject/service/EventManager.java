package telegram.bot.nure.botphonenumberprodject.service;

import org.jetbrains.annotations.NotNull;
import org.telegram.telegrambots.meta.api.objects.Update;
import telegram.bot.nure.botphonenumberprodject.events.Event;

import java.util.List;
import java.util.function.Predicate;

public interface EventManager {
    boolean put(@NotNull Long chatId, Event request);

    List<Event> remove(Long chatId);

    boolean removeIf(Long chatId, Predicate<Event> predicate);

    List<Event> get(Long chatId);

    void emit(Update update);
}
