package telegram.bot.nure.botphonenumberprodject.service;

import lombok.RequiredArgsConstructor;
import org.telegram.telegrambots.extensions.bots.commandbot.TelegramLongPollingCommandBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardRemove;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import telegram.bot.nure.botphonenumberprodject.handle.InvokeCommandMethod;
import telegram.bot.nure.botphonenumberprodject.tools.BotUtils;

import java.util.stream.Collectors;

@RequiredArgsConstructor
public class TelegramSettingServiceImpl implements TelegramSettingService {
    private final EventManager events;
    private final TelegramLongPollingCommandBot bot;
    private final InvokeCommandMethod commandMethod;

    @Override
    public void clearEvents(Long chatId) {
        var result = events.remove(chatId);
        String text = result == null ? "Event was empty" : "Event removed";

        BotUtils.sendMessage(bot, SendMessage.builder().text(text)
                .replyMarkup(new ReplyKeyboardRemove(true))
                .chatId(chatId).build());
    }

    @Override
    public void showAllCommand(Long chatId) {
        BotUtils.sendMessage(
                bot,
                SendMessage.builder().chatId(chatId)
                        .text(
                                commandMethod
                                        .getCommands()
                                        .stream()
                                        .sorted()
                                        .collect(Collectors.joining("\n")))
                        .build()
        );
    }
}
