package telegram.bot.nure.botphonenumberprodject.service;

import lombok.RequiredArgsConstructor;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardRemove;
import telegram.bot.nure.botphonenumberprodject.dataclass.User;
import telegram.bot.nure.botphonenumberprodject.entities.UserEntity;
import telegram.bot.nure.botphonenumberprodject.filter.UserUniqueFilter;
import telegram.bot.nure.botphonenumberprodject.tools.ContactReplyKeyboardBuilder;
import telegram.bot.nure.botphonenumberprodject.tools.UpdateUtils;

import java.time.LocalDateTime;

@RequiredArgsConstructor
public class TelegramBotAuthorizationServiceImpl implements TelegramBotAuthorizationService{
    private final UserService userService;
    private final ContactReplyKeyboardBuilder contactButtonBuilder;

    @Override
    public boolean isRegisteredUser(Update update) {
        return userService.findOne(
                UserUniqueFilter.builder()
                        .chatId(UpdateUtils.getChatId(update))
                        .build()
        ).isPresent();
    }

    @Override
    public SendMessage signUp(Update update) {
        if (update.getMessage().getContact() == null) {
            return SendMessage.builder()
                    .chatId(update.getMessage().getChatId())
                    .text("Please share contact")
                    .replyMarkup(contactButtonBuilder.build()).build();
        }
        userService.save(new User(
                update.getMessage().getChatId(),
                update.getMessage().getFrom().getId(),
                update.getMessage().getContact().getPhoneNumber().replace("+", ""),
                LocalDateTime.now(), true));

        return SendMessage.builder()
                .chatId(update.getMessage().getChatId())
                .replyMarkup(new ReplyKeyboardRemove(true))
                .text("Phone number is confirmed").build();
    }
}
