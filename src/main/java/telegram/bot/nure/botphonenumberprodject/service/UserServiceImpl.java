package telegram.bot.nure.botphonenumberprodject.service;

import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import telegram.bot.nure.botphonenumberprodject.dataclass.User;
import telegram.bot.nure.botphonenumberprodject.entities.UserEntity;
import telegram.bot.nure.botphonenumberprodject.filter.UserFilter;
import telegram.bot.nure.botphonenumberprodject.filter.UserUniqueFilter;
import telegram.bot.nure.botphonenumberprodject.mapper.UserMapper;
import telegram.bot.nure.botphonenumberprodject.repository.UserRepository;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository repository;
    private final UserMapper mapper;

    @Override
    @Caching(
            cacheable = {
                    @Cacheable(cacheNames = {"users"}, key = "#filter.phoneNumber", condition = "#filter.phoneNumber!=null"),
                    @Cacheable(cacheNames = {"users"}, key = "#filter.chatId", condition = "#filter.chatId!=null")
            }
    )
    public Optional<User> findOne(UserUniqueFilter filter) {
        return repository.findOne(filter).map(mapper::toUser);
    }

    @Override
    public List<User> findAll(UserFilter filter) {
        return repository.findAll(filter).stream().map(mapper::toUser).toList();
    }

    @Override
    @CachePut(value = "users", key = "#user.phoneNumber")
    public User update(User user) {
        return mapper.toUser(repository.update(mapper.toEntity(user)));
    }

    @Override
    @Caching(
            put = {
                    @CachePut(value = "users", key = "#user.phoneNumber"),
                    @CachePut(value = "users", key = "#user.chatId")
            }
    )
    public User save(User user) {
        return mapper.toUser(repository.save(mapper.toEntity(user)));
    }
}
