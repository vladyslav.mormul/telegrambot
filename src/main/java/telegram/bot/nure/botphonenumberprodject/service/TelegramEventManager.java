package telegram.bot.nure.botphonenumberprodject.service;

import org.jetbrains.annotations.NotNull;
import org.telegram.telegrambots.meta.api.objects.Update;
import telegram.bot.nure.botphonenumberprodject.events.Event;

import jakarta.annotation.PreDestroy;
import telegram.bot.nure.botphonenumberprodject.events.preparers.EventPreparable;
import telegram.bot.nure.botphonenumberprodject.tools.UpdateUtils;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Predicate;

public class TelegramEventManager implements EventManager {
    private final static Object PUT_MONITOR = new Object();
    private final Map<Long, List<Event>> events;
    private final Set<EventPreparable> eventPreparables;

    public TelegramEventManager(Set<EventPreparable> eventPreparables) {
        this.eventPreparables = eventPreparables;
        this.events = new ConcurrentHashMap<>();
    }

    @Override
    public boolean put(@NotNull Long chatId, Event request) {
        synchronized (PUT_MONITOR) {
            if (!events.containsKey(chatId)) {
                events.put(chatId, new ArrayList<>());
            }
        }
        eventPreparables.forEach(ep -> ep.prepare(this, chatId, request));
        return events.get(chatId).add(request);
    }

    @Override
    public List<Event> remove(Long chatId) {
        var result = events.remove(chatId);

        if (result != null) {
            result.forEach(Event::onDestroy);
        }

        return result;
    }

    @Override
    public boolean removeIf(Long chatId, Predicate<Event> predicate) {
        return events.get(chatId).removeIf(new DestroyPredicate(predicate));
    }


    @Override
    public List<Event> get(Long chatId) {
        return events.get(chatId).stream().toList();
    }

    @Override
    public void emit(Update update) {
        Long chatId = UpdateUtils.getChatId(update);
        events.getOrDefault(chatId, List.of()).stream().toList().forEach(e -> {
            if (e.isSuitable(update)) {
                e.emit(update);
            }
        });
    }

    @PreDestroy
    private void destroy() {
        events.forEach((id, es) -> es.forEach(Event::onDestroy));
    }

    private record DestroyPredicate(Predicate<Event> predicate) implements Predicate<Event> {

        @Override
        public boolean test(Event event) {
            var isDestroy = predicate.test(event);
            if (isDestroy) {
                event.onDestroy();
            }
            return isDestroy;
        }
    }
}
