package telegram.bot.nure.botphonenumberprodject.service;

import telegram.bot.nure.botphonenumberprodject.dataclass.User;
import telegram.bot.nure.botphonenumberprodject.entities.UserEntity;
import telegram.bot.nure.botphonenumberprodject.filter.UserFilter;
import telegram.bot.nure.botphonenumberprodject.filter.UserUniqueFilter;

import java.util.List;
import java.util.Optional;

public interface UserService {
    Optional<User> findOne(UserUniqueFilter filter);

    List<User> findAll(UserFilter filter);

    User update(User user);

    User save(User user);
}
