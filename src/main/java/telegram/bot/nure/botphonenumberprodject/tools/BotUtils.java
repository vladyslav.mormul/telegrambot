package telegram.bot.nure.botphonenumberprodject.tools;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.lang.NonNull;
import org.telegram.telegrambots.bots.DefaultAbsSender;
import org.telegram.telegrambots.extensions.bots.commandbot.TelegramLongPollingCommandBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.send.SendPhoto;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageReplyMarkup;
import org.telegram.telegrambots.meta.api.objects.InputFile;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.net.URL;
import java.util.Optional;

public class BotUtils {
    private static final Logger LOG = LoggerFactory.getLogger(BotUtils.class);

    public static boolean sendMessage(DefaultAbsSender bot, SendMessage message) {
        try {
            bot.execute(message);
            return true;
        } catch (TelegramApiException ex) {
            LOG.warn(ex.getMessage(), ex);
            return false;
        }
    }

    public static boolean updateKeyboard(DefaultAbsSender bot, Long chatId, Integer messageId, InlineKeyboardMarkup replyKeyboard) {
        try {
            bot.execute(EditMessageReplyMarkup.builder()
                    .chatId(chatId)
                    .messageId(messageId)
                    .replyMarkup(replyKeyboard)
                    .build());
            return true;
        } catch (TelegramApiException ex) {
            LOG.warn(ex.getMessage(), ex);
            return false;
        }
    }

    public static boolean sendRemoveKeyboard(DefaultAbsSender bot, Long chatId, Integer messageId) {
        return updateKeyboard(bot, chatId, messageId, null);
    }

    public static boolean trySend(DefaultAbsSender bot, TelegramSender sender) {
        try {
            sender.apply(bot);
            return true;
        } catch (TelegramApiException ex) {
            LOG.warn(ex.getMessage(), ex);
            return false;
        }
    }

    public static Optional<Message> trySendAndReturn(DefaultAbsSender bot, TelegramSenderAndReturn sender) {
        try {
            return Optional.ofNullable(sender.apply(bot));
        } catch (TelegramApiException ex) {
            LOG.warn(ex.getMessage(), ex);
            return Optional.empty();
        }
    }


    public static boolean sameCallBackMessage(@NonNull Update update, @NonNull Message message) {
        return sameCallBackMessage(update, message.getMessageId());
    }

    public static boolean sameCallBackMessage(@NonNull Update update, Integer messageId) {
        return update.getCallbackQuery().getMessage().getMessageId().equals(messageId);
    }


    public static SendPhoto.SendPhotoBuilder sendPhotoBuilderByUrl(String url, String name) {
        try {
            return SendPhoto.builder().photo(new InputFile(new URL(url).openStream(), name));
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }


    public interface TelegramSender {
        void apply(DefaultAbsSender bot) throws TelegramApiException;
    }

    public interface TelegramSenderAndReturn {
        Message apply(DefaultAbsSender bot) throws TelegramApiException;
    }
}
