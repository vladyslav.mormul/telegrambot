package telegram.bot.nure.botphonenumberprodject.exception;

import org.telegram.telegrambots.meta.api.methods.send.SendMessage;


public class TelegramValidException extends RuntimeException {
    private final SendMessage sendMessage;

    public TelegramValidException(SendMessage sendMessage, String message, Throwable throwable) {
        super(message, throwable);
        this.sendMessage = sendMessage;
    }

    public TelegramValidException(SendMessage sendMessage, String message) {
        this(sendMessage, message, null);
    }

    public TelegramValidException(SendMessage sendMessage) {
        this(sendMessage, "", null);

    }

    public SendMessage getSendMessage() {
        return sendMessage;
    }

    public boolean hasSendMessage() {
        return sendMessage != null;
    }
}
