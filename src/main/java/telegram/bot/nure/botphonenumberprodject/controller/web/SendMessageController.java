package telegram.bot.nure.botphonenumberprodject.controller.web;

import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import telegram.bot.nure.botphonenumberprodject.dto.SurveyTelegramMessageRequest;
import telegram.bot.nure.botphonenumberprodject.dto.TelegramMessageRequest;
import telegram.bot.nure.botphonenumberprodject.mapper.SurveyTelegramMessageMapper;
import telegram.bot.nure.botphonenumberprodject.mapper.TelegramMessageMapper;
import telegram.bot.nure.botphonenumberprodject.service.MessageService;
import telegram.bot.nure.botphonenumberprodject.validate.OnValidateForm;

import jakarta.validation.Valid;

@Validated
@RestController
@RequestMapping("/send-message")
@RequiredArgsConstructor
public class SendMessageController {
    private final MessageService messageService;
    private final TelegramMessageMapper messageMapper;
    private final SurveyTelegramMessageMapper surveyTelegramMessageMapper;

    @ResponseBody
    @Validated(OnValidateForm.class)
    @RequestMapping(value = "/text", method = RequestMethod.POST)
    public String sendTextMessage(@RequestBody @Valid TelegramMessageRequest request) {
        return messageService.sendMessage(messageMapper.toTelegramMessage(request)).name();
    }

    @ResponseBody
    @Validated(OnValidateForm.class)
    @RequestMapping(value = "/survey", method = RequestMethod.POST)
    public String sendSurveyMessage(@RequestBody @Valid SurveyTelegramMessageRequest request) {
        return messageService.sendMessage(surveyTelegramMessageMapper.toSurveyTelegramMessage(request)).name();
    }
}
