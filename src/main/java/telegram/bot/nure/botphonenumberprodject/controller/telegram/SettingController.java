package telegram.bot.nure.botphonenumberprodject.controller.telegram;

import lombok.RequiredArgsConstructor;
import org.telegram.telegrambots.meta.api.objects.Update;
import telegram.bot.nure.botphonenumberprodject.annotations.BotCommandController;
import telegram.bot.nure.botphonenumberprodject.annotations.BotCommandMapping;
import telegram.bot.nure.botphonenumberprodject.annotations.ChatId;
import telegram.bot.nure.botphonenumberprodject.service.TelegramSettingService;
import telegram.bot.nure.botphonenumberprodject.tools.UpdateUtils;

@BotCommandController
@RequiredArgsConstructor
public class SettingController {
    private final TelegramSettingService settingService;

    @BotCommandMapping(commands = "/clear")
    public void clear(@ChatId Long chatId) {
        settingService.clearEvents(chatId);
    }

    @BotCommandMapping(commands = "/help")
    public void showAllCommands(@ChatId Long chatId) {
        settingService.showAllCommand(chatId);
    }
}
