package telegram.bot.nure.botphonenumberprodject.postconfig;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.extensions.bots.commandbot.TelegramLongPollingCommandBot;
import telegram.bot.nure.botphonenumberprodject.bot.DefaultAbsSenderSetter;

@Component
@RequiredArgsConstructor
public class SetSenderRunner implements CommandLineRunner {
    private final ApplicationContext applicationContext;
    private final TelegramLongPollingCommandBot bot;

    @Override
    public void run(String... args) {
        applicationContext
                .getBeansOfType(DefaultAbsSenderSetter.class)
                .values()
                .forEach(s -> s.setBot(bot));
    }
}
