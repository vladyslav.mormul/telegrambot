package telegram.bot.nure.botphonenumberprodject.converters;

public interface StringConverter {
    <T> T map(String s, Class<T> clazz);
}
