package telegram.bot.nure.botphonenumberprodject.converters;

import org.telegram.telegrambots.meta.api.objects.Update;

public record UpdateParameterConverter(int index) implements ParameterConverter {
    @Override
    public int getIndex() {
        return index;
    }

    @Override
    public Object convert(Update update) {
        return update;
    }
}
