package telegram.bot.nure.botphonenumberprodject.converters;

import org.telegram.telegrambots.meta.api.objects.Update;

public interface ParameterConverter {
    int getIndex();

    Object convert(Update update);
}
