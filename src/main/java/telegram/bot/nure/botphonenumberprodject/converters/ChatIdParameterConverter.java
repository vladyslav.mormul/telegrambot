package telegram.bot.nure.botphonenumberprodject.converters;

import org.telegram.telegrambots.meta.api.objects.Update;
import telegram.bot.nure.botphonenumberprodject.tools.UpdateUtils;

public class ChatIdParameterConverter extends AbstractIndexHolderConverter {
    public ChatIdParameterConverter(int index) {
        super(index);
    }

    @Override
    public Object convert(Update update) {
        return UpdateUtils.getChatId(update);
    }
}