package telegram.bot.nure.botphonenumberprodject.converters;

import org.springframework.util.StringUtils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

public class TypeStringConverter implements StringConverter {
    private final Map<Class<?>, Function<String, ?>> valueToParameterClass = build();

    @Override
    public <T> T map(String s, Class<T> clazz) {
        return clazz.cast(valueToParameterClass.get(clazz).apply(s));
    }

    private static Map<Class<?>, Function<String, ?>> build() {
        return Map.ofEntries(
                Map.entry(Long.class, Long::valueOf),
                Map.entry(Integer.class, Integer::valueOf),
                Map.entry(Short.class, Short::valueOf),
                Map.entry(Byte.class, Byte::valueOf),
                Map.entry(String.class, String::valueOf),
                Map.entry(Double.class, Double::valueOf),
                Map.entry(Float.class, Float::valueOf),
                Map.entry(Character.class, s -> StringUtils.hasText(s) ? s.charAt(0) : null),
                Map.entry(LocalDateTime.class, LocalDateTime::parse),
                Map.entry(LocalTime.class, LocalTime::parse),
                Map.entry(LocalDate.class, LocalDate::parse)
        );
    }
}
