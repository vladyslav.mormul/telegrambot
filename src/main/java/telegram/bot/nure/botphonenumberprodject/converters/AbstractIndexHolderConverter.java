package telegram.bot.nure.botphonenumberprodject.converters;

import java.util.Objects;

public abstract class AbstractIndexHolderConverter implements ParameterConverter {
    private final int index;

    public AbstractIndexHolderConverter(int index) {
        this.index = index;
    }

    @Override
    public int getIndex() {
        return index;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AbstractIndexHolderConverter that = (AbstractIndexHolderConverter) o;
        return index == that.index;
    }

    @Override
    public int hashCode() {
        return Objects.hash(index);
    }
}
