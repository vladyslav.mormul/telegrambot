package telegram.bot.nure.botphonenumberprodject.converters;

import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import telegram.bot.nure.botphonenumberprodject.exception.TelegramValidException;
import telegram.bot.nure.botphonenumberprodject.tools.UpdateUtils;

import java.util.Objects;

public class CommandTextConverter extends AbstractIndexHolderConverter {
    private final boolean isRequired;
    private final String errorMessage;
    private final String defaultText;

    public CommandTextConverter(int index, boolean isRequired, String errorMessage, String defaultText) {
        super(index);
        this.isRequired = isRequired;
        this.errorMessage = errorMessage;
        this.defaultText = defaultText;
    }

    @Override
    public Object convert(Update update) {
        if (!update.hasMessage()) {
            return defaultText;
        }

        String text = update.getMessage().getText().trim();
        int index = text.indexOf(" ");

        if (index < 0 && isRequired) {
            throwMessage(update);
        }

        if (index < 0) {
            return defaultText;
        }

        return text.substring(index).trim();
    }

    private void throwMessage(Update update) {
        throw new TelegramValidException(
                SendMessage.builder()
                        .text(errorMessage)
                        .chatId(Objects.requireNonNull(UpdateUtils.getChatId(update)))
                        .build(),
                errorMessage
        );
    }
}
