package telegram.bot.nure.botphonenumberprodject.repository;

import telegram.bot.nure.botphonenumberprodject.entities.UserEntity;
import telegram.bot.nure.botphonenumberprodject.filter.UserFilter;
import telegram.bot.nure.botphonenumberprodject.filter.UserUniqueFilter;

import java.util.List;
import java.util.Optional;

public interface UserRepository {
    Optional<UserEntity> findOne(UserUniqueFilter filter);

    List<UserEntity> findAll(UserFilter filter);

    UserEntity update(UserEntity id);

    UserEntity save(UserEntity id);
}
