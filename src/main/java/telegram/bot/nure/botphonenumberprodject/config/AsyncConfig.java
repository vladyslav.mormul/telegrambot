package telegram.bot.nure.botphonenumberprodject.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

@Configuration
@EnableAsync
@PropertySource("classpath:properties/ThreadResources.properties")
public class AsyncConfig {

    @Bean
    public ThreadPoolTaskExecutor telegramRequestExecutor(
            @Value("${thread.pool.size.core}") int corePoolSize,
            @Value("${thread.pool.size.max}") int maxPoolSize,
            @Value("${thread.queue.capacity.size}") int queueCapacity,
            @Value("${thread.name.prefix}") String threadNamePrefix
    ) {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(corePoolSize);
        executor.setMaxPoolSize(maxPoolSize);
        executor.setQueueCapacity(queueCapacity);
        executor.setThreadNamePrefix(threadNamePrefix);
        executor.initialize();
        return executor;
    }
}
