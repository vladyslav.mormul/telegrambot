package telegram.bot.nure.botphonenumberprodject.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import telegram.bot.nure.botphonenumberprodject.events.preparers.AbstractHelperEventPrepare;
import telegram.bot.nure.botphonenumberprodject.events.preparers.EventPreparable;

@Configuration
public class EventPreparableConfig {

    @Bean
    EventPreparable abstractHelperEventPrepare() {
        return new AbstractHelperEventPrepare();
    }
}
