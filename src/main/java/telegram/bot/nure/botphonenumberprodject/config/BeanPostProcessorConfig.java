package telegram.bot.nure.botphonenumberprodject.config;

import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import telegram.bot.nure.botphonenumberprodject.beans.BotCommandMappingAndControllerBeanPostProcessor;
import telegram.bot.nure.botphonenumberprodject.events.preparers.EventPreparable;
import telegram.bot.nure.botphonenumberprodject.handle.InvokeCommandMethod;
import telegram.bot.nure.botphonenumberprodject.handle.InvokeCommandMethodImpl;
import telegram.bot.nure.botphonenumberprodject.service.EventManager;
import telegram.bot.nure.botphonenumberprodject.service.TelegramEventManager;

import java.util.Set;

@Configuration
public class BeanPostProcessorConfig {

    @Bean
    InvokeCommandMethod InvokeCommandMethodImpl() {
        return new InvokeCommandMethodImpl();
    }

    @Bean
    EventManager messageEventsImpl(Set<EventPreparable> eventPreparables) {
        return new TelegramEventManager(eventPreparables);
    }

    @Bean
    BeanPostProcessor botCommandMapperAndControllerBeanPostProcessor(
            ApplicationContext context, InvokeCommandMethod commandMethod, Environment env) {
        return new BotCommandMappingAndControllerBeanPostProcessor(context, commandMethod, env);
    }
}
