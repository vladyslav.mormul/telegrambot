package telegram.bot.nure.botphonenumberprodject.dataclass;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@EqualsAndHashCode(exclude = "dateRegistration")
@AllArgsConstructor
@NoArgsConstructor
public class User {
    private Long id;
    private Long chatId;
    private String phoneNumber;
    private LocalDateTime dateRegistration;
    private boolean active;
}
