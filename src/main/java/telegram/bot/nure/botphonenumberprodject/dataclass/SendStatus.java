package telegram.bot.nure.botphonenumberprodject.dataclass;

public enum SendStatus {
    SENT, DELAY_SENT, ERROR
}
