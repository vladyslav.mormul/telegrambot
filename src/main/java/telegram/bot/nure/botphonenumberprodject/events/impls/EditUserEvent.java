package telegram.bot.nure.botphonenumberprodject.events.impls;

import lombok.RequiredArgsConstructor;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardRemove;
import telegram.bot.nure.botphonenumberprodject.dataclass.User;
import telegram.bot.nure.botphonenumberprodject.entities.UserEntity;
import telegram.bot.nure.botphonenumberprodject.events.AbstractHelperEvent;
import telegram.bot.nure.botphonenumberprodject.filter.UserUniqueFilter;
import telegram.bot.nure.botphonenumberprodject.service.UserService;
import telegram.bot.nure.botphonenumberprodject.tools.BotUtils;

@RequiredArgsConstructor
public class EditUserEvent extends AbstractHelperEvent {
    private static final String DONE = "Editing is done";
    private static final String ERROR = "Editing ended with an error";
    private final UserService userService;

    @Override
    public boolean isSuitable(Update update) {
        return update.hasMessage();
    }

    @Override
    public void emit(Update update) {
        if (update.getMessage().getContact() == null) {
            stopSelf();
            turnOffButton(ERROR);
            return;
        }

        var user = getUser();

        user.setPhoneNumber(update.getMessage().getContact().getPhoneNumber());

        userService.update(user);
        turnOffButton(DONE);
    }

    private void turnOffButton(String text) {
        BotUtils.sendMessage(getTelegramSender(),
                SendMessage.builder()
                        .chatId(getChatId())
                        .replyMarkup(new ReplyKeyboardRemove(true))
                        .text(text).build()
        );
    }

    private User getUser() {
        return userService.findOne(
                UserUniqueFilter
                        .builder()
                        .chatId(getChatId())
                        .build()
        ).orElseThrow();
    }
}
