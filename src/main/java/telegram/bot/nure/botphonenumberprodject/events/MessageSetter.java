package telegram.bot.nure.botphonenumberprodject.events;

import org.telegram.telegrambots.meta.api.objects.Message;

public interface MessageSetter {
    void setMessage(Message message);
}
