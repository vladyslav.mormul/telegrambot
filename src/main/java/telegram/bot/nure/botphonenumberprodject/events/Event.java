package telegram.bot.nure.botphonenumberprodject.events;

import org.telegram.telegrambots.meta.api.objects.Update;

public interface Event {

    default boolean isSuitable(Update update) {
        return true;
    }

    void emit(Update update);

    default void onDestroy() {
    }
}
