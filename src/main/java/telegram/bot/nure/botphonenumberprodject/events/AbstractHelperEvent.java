package telegram.bot.nure.botphonenumberprodject.events;

import org.telegram.telegrambots.bots.DefaultAbsSender;
import telegram.bot.nure.botphonenumberprodject.service.EventManager;

public abstract class AbstractHelperEvent implements Event {
    private DefaultAbsSender telegramSender;
    private EventManager eventManager;
    private Long chatId;

    public final Long getChatId() {
        return chatId;
    }

    protected final void stopSelf() {
        eventManager.removeIf(chatId, this::equals);
    }

    protected final DefaultAbsSender getTelegramSender() {
        return telegramSender;
    }

    protected final void startEvent(Event event) {
        eventManager.put(chatId, event);
        stopSelf();
    }
}
