package telegram.bot.nure.botphonenumberprodject.events.impls;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RequestMethod;
import org.telegram.telegrambots.extensions.bots.commandbot.TelegramLongPollingCommandBot;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.DeleteMessage;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageText;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import telegram.bot.nure.botphonenumberprodject.client.HttpClient;
import telegram.bot.nure.botphonenumberprodject.events.Event;
import telegram.bot.nure.botphonenumberprodject.events.MessageSetter;
import telegram.bot.nure.botphonenumberprodject.mapper.HttpRequestParametersMapper;
import telegram.bot.nure.botphonenumberprodject.service.EventManager;
import telegram.bot.nure.botphonenumberprodject.tools.BotUtils;
import telegram.bot.nure.botphonenumberprodject.tools.HttpCallBack;

import java.util.Objects;

@RequiredArgsConstructor
public class SurveyEvent implements Event, MessageSetter {
    private final HttpCallBack callBack;
    private final TelegramLongPollingCommandBot bot;
    private final EventManager events;
    private final HttpClient httpClient;
    private final HttpRequestParametersMapper requestParametersMapper;

    private Message message;

    @Override
    public boolean isSuitable(Update update) {
        return BotUtils.sameCallBackMessage(update, message);
    }

    @Override
    public void emit(Update update) {
        var httpRequestParameters = requestParametersMapper.toHttpRequestParameters(
                callBack,
                update.getCallbackQuery().getData()
        );

        if (RequestMethod.GET.equals(callBack.getMethod())) {
            httpClient.get(httpRequestParameters);
        } else {
            httpClient.post(httpRequestParameters);
        }

        completeSurvey();
    }

    @Override
    public void setMessage(Message message) {
        this.message = message;
    }

    @Override
    public void onDestroy() {
        BotUtils.trySend(bot, b ->
                b.execute(DeleteMessage.builder()
                        .chatId(message.getChatId())
                        .messageId(message.getMessageId())
                        .build())
        );
    }

    private void completeSurvey() {
        BotUtils.trySend(bot, b ->
                b.execute(EditMessageText
                        .builder()
                        .chatId(message.getChatId())
                        .messageId(message.getMessageId())
                        .text(message.getText() + " ✅")
                        .build())
        );
        events.removeIf(message.getChatId(), this::equals);
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SurveyEvent that = (SurveyEvent) o;

        return Objects.equals(callBack, that.callBack) && Objects.equals(message, that.message);
    }

    @Override
    public int hashCode() {
        return Objects.hash(callBack, message);
    }
}
