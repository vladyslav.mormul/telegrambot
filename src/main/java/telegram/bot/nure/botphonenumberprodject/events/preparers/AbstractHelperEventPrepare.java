package telegram.bot.nure.botphonenumberprodject.events.preparers;

import lombok.RequiredArgsConstructor;
import org.springframework.lang.NonNull;
import org.springframework.util.ReflectionUtils;
import org.telegram.telegrambots.bots.DefaultAbsSender;
import telegram.bot.nure.botphonenumberprodject.bot.DefaultAbsSenderSetter;
import telegram.bot.nure.botphonenumberprodject.events.AbstractHelperEvent;
import telegram.bot.nure.botphonenumberprodject.events.Event;
import telegram.bot.nure.botphonenumberprodject.service.EventManager;

import java.lang.reflect.Field;
import java.util.Objects;

public class AbstractHelperEventPrepare implements EventPreparable, DefaultAbsSenderSetter {
    private DefaultAbsSender defaultAbsSender;
    @NonNull
    private static final Field TELEGRAM_SENDER_FIELD = Objects.requireNonNull(
            ReflectionUtils.findField(AbstractHelperEvent.class, "telegramSender", DefaultAbsSender.class)
    );
    @NonNull
    private static final Field EVENT_MANAGER_FIELD = Objects.requireNonNull(
            ReflectionUtils.findField(AbstractHelperEvent.class, "eventManager", EventManager.class)
    );
    @NonNull
    private static final Field CHAT_ID_FIELD = Objects.requireNonNull(
            ReflectionUtils.findField(AbstractHelperEvent.class, "chatId", Long.class)
    );

    static {
        TELEGRAM_SENDER_FIELD.setAccessible(true);
        EVENT_MANAGER_FIELD.setAccessible(true);
        CHAT_ID_FIELD.setAccessible(true);
    }

    @Override
    public void prepare(EventManager eventManager, Long chatId, Event event) {
        if (!(event instanceof AbstractHelperEvent)) {
            return;
        }
        setTelegramSender(event);
        setEventManager(eventManager, event);
        setChatId(event, chatId);
    }

    private void setTelegramSender(Event event) {
        ReflectionUtils.setField(TELEGRAM_SENDER_FIELD, event, defaultAbsSender);
    }

    private void setEventManager(EventManager eventManager, Event event) {
        ReflectionUtils.setField(EVENT_MANAGER_FIELD, event, eventManager);
    }

    private void setChatId(Event event, Long chatId) {
        ReflectionUtils.setField(CHAT_ID_FIELD, event, chatId);
    }

    @Override
    public void setBot(DefaultAbsSender sender) {
        this.defaultAbsSender = sender;
    }
}
