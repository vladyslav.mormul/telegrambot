package telegram.bot.nure.botphonenumberprodject.events.preparers;

import telegram.bot.nure.botphonenumberprodject.events.Event;
import telegram.bot.nure.botphonenumberprodject.service.EventManager;

public interface EventPreparable {
    void prepare(EventManager eventManager, Long chatId, Event event);
}
