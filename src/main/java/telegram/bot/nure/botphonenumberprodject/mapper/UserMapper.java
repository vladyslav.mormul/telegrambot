package telegram.bot.nure.botphonenumberprodject.mapper;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import telegram.bot.nure.botphonenumberprodject.dataclass.User;
import telegram.bot.nure.botphonenumberprodject.entities.UserEntity;

@Component
public class UserMapper {
    private final ModelMapper mapper = new ModelMapper();

    public UserEntity toEntity(User user) {
        return mapper.map(user, UserEntity.class);
    }

    public User toUser(UserEntity entity) {
        return mapper.map(entity, User.class);
    }
}
