package telegram.bot.nure.botphonenumberprodject.finder;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.context.ApplicationContext;

import java.lang.reflect.Parameter;

@RequiredArgsConstructor
public class BeanFinder implements ParameterFinder {
    private final ApplicationContext context;

    @Override
    public Result find(Parameter parameter, int index) {
        try {
            var bean = context.getBean(parameter.getType());
            return new Result(true, bean);
        } catch (NoSuchBeanDefinitionException e) {
            return Result.NOT_FOUND;
        }
    }
}
