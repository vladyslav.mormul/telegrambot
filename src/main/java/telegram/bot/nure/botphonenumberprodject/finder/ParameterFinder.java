package telegram.bot.nure.botphonenumberprodject.finder;

import telegram.bot.nure.botphonenumberprodject.converters.ParameterConverter;

import java.lang.reflect.Parameter;

public interface ParameterFinder {

    Result find(Parameter parameter, int index);

    record Result(boolean isSuccess, Object value, ParameterConverter converter) {
        public static final Result NOT_FOUND = new Result(false, null);

        public Result(boolean isSuccess, Object value) {
            this(isSuccess, value, null);
        }

        public Result(boolean isSuccess, ParameterConverter converter) {
            this(isSuccess, null, converter);
        }

        public boolean hasConverter() {
            return converter != null;
        }
    }
}
