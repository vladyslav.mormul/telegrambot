package telegram.bot.nure.botphonenumberprodject.finder;

import telegram.bot.nure.botphonenumberprodject.annotations.CommandText;
import telegram.bot.nure.botphonenumberprodject.converters.CommandTextConverter;

import java.lang.reflect.Parameter;

public class CommandTextFinder implements ParameterFinder {
    @Override
    public Result find(Parameter parameter, int index) {
        var annotation = parameter.getAnnotation(CommandText.class);
        if (annotation == null) {
            return Result.NOT_FOUND;
        }

        return new Result(true, new CommandTextConverter(
                index,
                annotation.required(),
                annotation.message(),
                annotation.defaultText()
        ));
    }
}
