package telegram.bot.nure.botphonenumberprodject.finder;

import org.telegram.telegrambots.meta.api.objects.Update;
import telegram.bot.nure.botphonenumberprodject.converters.UpdateParameterConverter;

import java.lang.reflect.Parameter;

public class UpdateFinder implements ParameterFinder {
    @Override
    public Result find(Parameter parameter, int index) {
        if (!parameter.getType().isAssignableFrom(Update.class)) {
            return Result.NOT_FOUND;
        }

        return new Result(true, new UpdateParameterConverter(index));
    }
}
