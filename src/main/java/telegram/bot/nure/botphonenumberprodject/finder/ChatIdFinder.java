package telegram.bot.nure.botphonenumberprodject.finder;

import telegram.bot.nure.botphonenumberprodject.annotations.ChatId;
import telegram.bot.nure.botphonenumberprodject.converters.ChatIdParameterConverter;

import java.lang.reflect.Parameter;

public class ChatIdFinder implements ParameterFinder {
    @Override
    public Result find(Parameter parameter, int index) {
        if (parameter.getAnnotation(ChatId.class) == null) {
            return Result.NOT_FOUND;
        }

        return new Result(true, new ChatIdParameterConverter(index));
    }
}
