package telegram.bot.nure.botphonenumberprodject.finder;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;

import java.lang.reflect.Parameter;

@RequiredArgsConstructor
public class QualifierBeanFinder implements ParameterFinder {
    private final ApplicationContext context;

    @Override
    public Result find(Parameter parameter, int index) {
        var annotation = parameter.getAnnotation(Qualifier.class);

        if (annotation == null) {
            return Result.NOT_FOUND;
        }

        return new Result(true, context.getBean(annotation.value()));
    }
}
