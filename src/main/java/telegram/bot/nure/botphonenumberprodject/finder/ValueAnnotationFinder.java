package telegram.bot.nure.botphonenumberprodject.finder;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import telegram.bot.nure.botphonenumberprodject.converters.StringConverter;

import java.lang.reflect.Parameter;
import java.util.function.Function;

@RequiredArgsConstructor
public class ValueAnnotationFinder implements ParameterFinder {
    private final StringConverter stringMapper;
    private final Function<String, String> toValue;

    @Override
    public Result find(Parameter parameter, int index) {
        var annotation = parameter.getAnnotation(Value.class);
        if (annotation == null) {
            return Result.NOT_FOUND;
        }

        return new Result(true, stringMapper.map(
                toValue.apply(annotation.value()),
                parameter.getType()
        ));
    }
}
