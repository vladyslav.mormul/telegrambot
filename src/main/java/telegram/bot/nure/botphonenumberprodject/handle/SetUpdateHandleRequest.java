package telegram.bot.nure.botphonenumberprodject.handle;

import org.springframework.util.ReflectionUtils;
import org.telegram.telegrambots.meta.api.objects.Update;
import telegram.bot.nure.botphonenumberprodject.converters.ParameterConverter;

import java.lang.reflect.Method;
import java.util.Collections;
import java.util.List;

public record SetUpdateHandleRequest(
        Method method,
        Object controller,
        Object[] parameters,
        List<ParameterConverter> parameterConverters
) implements HandleRequest {

    public SetUpdateHandleRequest {
        parameterConverters = parameterConverters.isEmpty() ? Collections.emptyList() : parameterConverters;
    }

    @Override
    public Object apply(Update update) {
        var putParameters = parameters.clone();
        parameterConverters.forEach(pc -> putParameters[pc.getIndex()] = pc.convert(update));

        return ReflectionUtils.invokeMethod(method, controller, putParameters);
    }
}
