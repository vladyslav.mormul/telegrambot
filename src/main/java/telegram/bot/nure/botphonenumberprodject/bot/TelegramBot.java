package telegram.bot.nure.botphonenumberprodject.bot;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.telegram.telegrambots.extensions.bots.commandbot.TelegramLongPollingCommandBot;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import telegram.bot.nure.botphonenumberprodject.exception.TelegramValidException;
import telegram.bot.nure.botphonenumberprodject.handle.UserMessageHandle;
import telegram.bot.nure.botphonenumberprodject.tools.BotUtils;


public class TelegramBot extends TelegramLongPollingCommandBot {
    private static final Logger LOGGER = LoggerFactory.getLogger(TelegramBot.class);
    private final String username;
    private final UserMessageHandle messageHandle;
    private final ThreadPoolTaskExecutor executor;

    public TelegramBot(String username, String token, UserMessageHandle messageHandle, ThreadPoolTaskExecutor executor) {
        super(token);
        this.username = username;
        this.messageHandle = messageHandle;
        this.executor = executor;
    }

    @Override
    public String getBotUsername() {
        return username;
    }

    @Override
    public void processNonCommandUpdate(Update update) {
        executor.submit(() -> {
            try {
                var resultMessage = messageHandle.handle(update);

                resultMessage.ifPresent(
                        mes -> BotUtils.sendMessage(this, mes)
                );

            } catch (TelegramValidException e) {
                handleTelegramValidException(e);
            } catch (Exception e) {
                LOGGER.error("Error with execute telegram request ", e);
            }
        });
    }

    private void handleTelegramValidException(TelegramValidException e) {
        LOGGER.warn(e.getMessage(), e);
        if (e.hasSendMessage()) {
            try {
                execute(e.getSendMessage());
            } catch (TelegramApiException ex) {
                LOGGER.error("Telegram Valid Exception has error with execute telegram request ", e);
            }
        }
    }
}
