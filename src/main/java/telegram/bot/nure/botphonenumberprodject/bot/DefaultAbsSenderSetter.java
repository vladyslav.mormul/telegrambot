package telegram.bot.nure.botphonenumberprodject.bot;

import org.telegram.telegrambots.bots.DefaultAbsSender;

public interface DefaultAbsSenderSetter {
    void setBot(DefaultAbsSender sender);
}
